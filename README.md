# Curso_nodeJSBalta

# NodeJS

Node.js é um interpretador de código JavaScript que funciona do lado do servidor. Seu objetivo é ajudar programadores na criação de aplicações de alta escalabilidade (como um servidor web), com códigos capazes de manipular milhares conexões.

# Criando APIs com Node
Neste curso vamos unir a popularidade das APIs com a popularidade do JavaScript criando uma API completa com NodeJs, passando pelos principais pontos que você precisa conhecer para colocar seu projeto em produção.

# Introdução e Configuração
    * Instalação Node, NPM e VS Code
    * npm init e instalação dos pacotes
    * Criando um servidor Web
    * Normalizando a porta
    * Gerenciando Erros do Servidor
    * Iniciando o Debug
    * Separando o Servidor
    * Configurando o NPM Start
    * Nodemon
 
# REST e CRUD
    * CRUD REST
    * Rotas
    * Controllers
    * MongoDb Setup
    * Mongoose
    * Models
    * Criando um Produto
    * Listando os Produtos
    * Listando um Produto pelo slug
    * Listando um Produto pelo Id
    * Listando os Produtos de uma tag
    * Atualizando um produto
    * Excluindo um produto
    * Validações
    * Repositórios
    * Async/Await
    * Revisitando os Models: Customer
    * Revisitando os Models: Order
    * Revisitando os Controllers: Customer
    * Revisitando os Controllers: Order
   
# Melhorando a API
    * Arquivo de Configurações
    * Encriptando a senha
    * Enviando E-mail de Boas Vindas
    * Upload da Imagem do Produto
  
# Segurança
    * Autenticação
    * Recuperando dados do usuário logado
    * Refresh Token
    * Autorização
 
 # Outros
    * Outros
    * Publicando a API
    * Conclusão
